<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'wp_desafio_web2duo' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'root' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '{5ZyF`~bV5BP6]UfCXkht4z}zj?_AGj~v^]?_1UokHxX|5[+|h5>5/a1VGy6Pf:3' );
define( 'SECURE_AUTH_KEY',  'E@Ohc5f3lwf{Gk{wf^I2Wy 3#5J=?ON&G*ElxG&}&d1R$.e6HtZKC96G.AORWvu}' );
define( 'LOGGED_IN_KEY',    'Zr2-LEMsb%ZAxQ{213:}a?+NH9BUI!]Mza:O.9&_,wzHL^(UYOF7V]PNP+iz.J>W' );
define( 'NONCE_KEY',        'gRw)zb)5r)(w=Z1xRr3rCoH-gyFNS=?dqzs=4a37;9QM`<7t4w^hv=(pa>.KS{f?' );
define( 'AUTH_SALT',        '$GEcvM!yZhZGVNuz*%E<Zf+GD#*8o?U*Mfk: U_SES3:N|xlY3h)KK+,+28P(YK5' );
define( 'SECURE_AUTH_SALT', 'vnyo:* `}F>T(5`Xr;J=*DD};2#Qw*|Rom;W!DmTW|ZUbGYXntwBi`EG%5YP3Zy<' );
define( 'LOGGED_IN_SALT',   '!N~ uLQSAdNM^e/SzTsaC%#MLEE[Io[6e>LAX[;h.<M2ux)Ku+>T%JObIAspwN$4' );
define( 'NONCE_SALT',       ',OL~Mr/cUR9dHd_pws%W?&(YsAP!oT%&;kc-tfU+yatE%pqup:%Rjxn^!%qcK=)k' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_web2duo';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
