 <?php
/**
 * 
 * Template Name: Home
 * page-home.php
 *
 * Desenvolvido por ANAWD - Desenvolvimento Web
 * Copyright © Todos os direitos reservados
 *
 * 
 */
 ?>
<?php get_header(); ?>



<section class="home-banner">
  <div class="container-fluid p-0">
    <div class="row">
      <div class="col">
        <img src="<?php bloginfo('template_directory');?>/assets/images/banner1.png">
      </div>
    </div>
  </div>
</section>

<section class="home-produtos">
 <div class="container">
   <div class="row">
      <div class="col-12 col-lg-4 col-xl-4">
          <div class="box-image-produto">
            <img src="<?php bloginfo('template_directory');?>/assets/images/image-produto.png">
            <div class="text-desc-lateral">
                <a href="#">Preencha a proposta de adesão</a>
            </div>
          </div>
      </div>
      <div class="col-12 col-lg-8 col-xl-8">
        <div class="row mb-3">
          <div class="col-12 col-lg-6 col-xl-6">
          <div class="box-image-produto">
            <img src="<?php bloginfo('template_directory');?>/assets/images/image-produto1.jpg">
            <div class="text-desc beleza">
                <h3>Beleza</h3>
                <h2>Ruiva Total</h2>
                <p>Que o ruivo é o tom do momento <br/>todo mundo sabe.</p>
                <a href="#">Saiba mais</a>
            </div>
          </div>
          </div>
          <div class="col-12 col-lg-6 col-xl-6">
          <div class="box-image-produto">
            <img src="<?php bloginfo('template_directory');?>/assets/images/image-produto2.jpg">
            <div class="text-desc bob-sponja">
                <h2> Promoção <br/>Bob Esponja</h2>
                <p>Na compra de uma peça Bob Esponja, ganhe um brinde. </p>
                <a href="#">Continue lendo</a>
            </div>
          </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-lg-6 col-xl-6">
            <div class="box-image-produto">
                <img src="<?php bloginfo('template_directory');?>/assets/images/image-produto3.jpg">
                <div class="text-desc moda">
                  <h3>Moda logo</h3>
                  <h2> Paixão <br/>por Jeans</h2>
                  <p>Versátil, combina com vários estilos diferentes.</p>
                  <a href="#">Saiba mais</a>
                </div>
            </div>
          </div>
          <div class="col-12 col-lg-6 col-xl-6">
            <div class="box-image-produto">
              <img src="<?php bloginfo('template_directory');?>/assets/images/image-produto4.jpg">
              <div class="text-desc beleza1">
                  <h2>  Poder <br/>Instantâneo</h2>
                  <p>Batom vermelho deixa toda mulher poderosa.</p>
                  <a href="#">Saiba mais</a>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
 </div>
</section>

<section class="mapa">
  <div class="container-fluid">
    <div class="col">
      <img src="<?php bloginfo('template_directory');?>/assets/images/mapa.jpg" alt="Mapa">
    </div>
  </div>
</section>

<section class="home-newsletter">
   <div class="container">
    <div class="row align-items-center">
      <div class="col-12 col-lg-6 col-xl-6">
          <h3>Assine a newsletter dologo</h3>
          <div class="input-group input-newsletter">
              <input type="text" class="form-control" placeholder="Seu email" aria-label="Seu email" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="button">Enviar</button>
              </div>
          </div>
      </div>
      <div class="col-12 col-lg-6 col-xl-6">
        <h3>Siga lojas logos nas redes sociais</h3>
        <div class="redes-sociais">
          <a href="#" title="Facebook">
            <img src="<?php bloginfo('template_directory');?>/assets/images/icon-face.jpg" alt="Facebook">
          </a>
          <a href="#" title="Youtube">
            <img src="<?php bloginfo('template_directory');?>/assets/images/icon-youtube.jpg" alt="Youtube">
          </a>
          <a href="#" title="Instagram">
            <img src="<?php bloginfo('template_directory');?>/assets/images/icon-insta.jpg" alt="Instagram">
          </a>
        </div>
      </div>
    </div>
   </div>
</section>





<?php get_footer(); ?>