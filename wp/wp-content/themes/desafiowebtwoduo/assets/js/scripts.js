jQuery(function() {
    jQuery('.bannerSlider').slick({
        dots: true,
	    autoplay: true,
	    autoplaySpeed: 5000,
	    infinite: true,
	    slidesToShow: 1,
	    slideswToScroll: 1
    });
});