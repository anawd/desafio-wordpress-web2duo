<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?php bloginfo('name'); ?><?php wp_title('|'); ?></title>
	<meta name="description" content="<?php bloginfo('description'); ?>">

	<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Maven+Pro:wght@400;500;700;900&display=swap" rel="stylesheet">

	<?php wp_head(); ?>
	
</head>
<body <?php body_class(); ?>>
<div id="page" class="site">

		<header class="site-header">
			<div class="container-fluid topMenu">
				<div class="row">
					<div class="col d-flex justify-content-center align-items-center">
						<div class="linkMenuTop">
							<a href="#">Peça seu Cartão de Cliente</a>
							<a href="#">Solicite a 2ª via do boleto</a>
							<a href="#">Encontre uma loja</a>
							<a href="#">Assine a newsletter</a>
						</div>
					</div>
				</div>
			</div>
			<div class="container mainMenu">
				<div class="row">
					<div class="col d-flex justify-content-between align-items-end">
						<div class="logo">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
								<img src="<?php bloginfo('template_directory');?>/assets/images/logo.png" class="mx-auto"
								>
							</a>
						</div>
						
						<div class="menu">
							<nav class="navMain">
							    <?php
									wp_nav_menu(array(
										'theme_location'  => 'menu_principal',
										'menu_class'      => 'mainMenu',
										'container'       => false
									));
								?>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</header>

<div id="content" class="site-content">