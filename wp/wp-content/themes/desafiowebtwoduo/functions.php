<?php 

// Função para carregamento dos scripts
function carrega_scripts(){

    //  BOOTSTRAP
    wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/assets/lib/bootstrap/bootstrap.min.css', array(), '4.0.0', 'all');
    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/assets/lib/bootstrap/bootstrap.min.js', array('jquery' ), null, true);
    wp_enqueue_script( 'bootstrap-popper', get_template_directory_uri() . '/assets/lib/bootstrap/popper.min.js', array('jquery' ), null, true);

    //SLICK
    wp_enqueue_style( 'slick-css', get_template_directory_uri() . '/assets/lib/slick/slick.css', array(), '', 'all');
    wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/assets/lib/slick/slick-theme.css', array(), '', 'all');
    wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/assets/lib/slick/slick.js', array(), null, true);



	// CSS
	wp_enqueue_style( 'estilo-css', get_template_directory_uri() . '/assets/css/estilo.css', array(), '1.0', 'all');
	
    //JS
	wp_enqueue_script( 'scripts', get_template_directory_uri(). '/assets/js/scripts.js', array(), null, true);	
}
add_action( 'wp_enqueue_scripts', 'carrega_scripts' );

// Função para registro de nossos menus
register_nav_menus(
	array(
		'menu_principal' => 'Menu Principal',
        'menu_mobile' => 'Menu Mobile',
	)
);

// Adicionando suporte ao tema
add_theme_support('custom-background');
add_theme_support('custom-header');
add_theme_support('post-thumbnails');
add_theme_support('post-formats', array('video', 'image'));
add_theme_support('html5', array('search-form'));


/* LIMITADOR DE CARACTERES */
function the_excerpt_max_charlength($charlength) {
    
    $excerpt = get_the_excerpt();
    $charlength++;

    if ( mb_strlen( $excerpt ) > $charlength ) {
        $subex = mb_substr( $excerpt, 0, $charlength - 5 );
        $exwords = explode( ' ', $subex );
        $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
        if ( $excut < 0 ) {
            echo mb_substr( $subex, 0, $excut );
        } else {
            echo $subex;
        }
        echo '...';
    } else {
        echo $excerpt;
    }
}

/*************************************************************/
/****** | IMAGE SIZES
/*************************************************************/
add_image_size('size1920x640', 1920, 640, true ); // Imagem Destacada Página Interna


class Menu_With_Description extends Walker_Nav_Menu {
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
         
        $class_names = $value = '';
 
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
 
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="' . esc_attr( $class_names ) . '"';
 
        $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';
 
        $attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
        $attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
        $attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';
 
        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '<br /><span class="sub">' . $item->description . '</span>';
        $item_output .= '</a>';
        $item_output .= $args->after;
 
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}

/**
 * Register Custom Navigation Walker
 */
function register_navwalker(){
    require_once get_template_directory() . '/assets/inc/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );


 ?>