<?php
/**
 * The template for displaying the footer
 *
 */

?>

</div><!-- #content -->


		<footer class="site-footer">
			<div class="container">
				<div class="row align-items-start">
					<div class="col col-2">
						<h4>Lojas Logo</h4>
						<ul>
							<li><a href="#">Sobre</a></li>
							<li><a href="#">Lojas</a></li>
							<li><a href="#">Trabalhe Conosco</a></li>
							<li><a href="#">Contato</a></li>
						</ul>					
					</div>
					<div class="col col-2">
						<h4>Lista de Atalhos</h4>
						<ul>
							<li><a href="#">Portal do Colaborador</a></li>
							<li><a href="#">Promoções</a></li>
							<li><a href="#">Cartão do Cliente</a></li>
							<li><a href="#">Cadastre-se</a></li>
							<li><a href="#">Blog</a></li>
						</ul>
					</div>
					<div class="col col-3">
						<h4>Sac Loja Logo</h4>
						<h4>0800-701-0316</h4>
						<a href="#"><img src="<?php bloginfo('template_directory');?>/assets/images/btn-segunda-via.png"></a>
					</div>
					<div class="col col-5">
						
					</div>
				</div>
			</div>
		</footer><!-- .site-footer -->

	
</div><!-- #page -->


<?php wp_footer(); ?>



</body>
</html>
